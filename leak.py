import requests
import tweepy
import time
import urllib.request
import PIL
from PIL import Image, ImageFont, ImageDraw
import os
import json
from colorama import *
init()

loop = True
count = 1
initialCheckDelay = 2
currentVersion = '1.0.1'

response = requests.get('http://ifbr.xyz/api/devtools/autoleak/newsData.json')
ln1 = response.json()["1"]
ln2 = response.json()["2"]
ln3 = response.json()["3"]
ln4 = response.json()["4"]
ln5 = response.json()["5"]
latestVersion = response.json()["latestVersion"]
print("")
print("------------------------------------------------------------------------------------------------")
print("")
print(ln1)                 #############################################################################
print(ln2)                 #  DO NOT REMOVE THESE LINES OF CODE!                                       #
print(ln3)                 #  IT IS UESD TO COMMUNICATE UPDATES WITH YOU WHEN YOU LAUNCH THE PROGRAM!  #
print(ln4)                 #  IF YOU REMOVE IT YOU WILL NOT BE ALERTED WITH NEWS AND NEW UPDATES!      #
print(ln5)                 #############################################################################
print("")
print("------------------------------------------------------------------------------------------------")
print("")
print("Version info:")
print("")
if latestVersion == currentVersion:
    print('--> This version of AUTOLEAK is up to date!')
else:
    print('--> You are currently running v'+currentVersion+' of AutoLeak, v'+latestVersion+' is now avaliable - Please check the pinned post at https://www.buymeacoffee.com/thomaskeig/posts for the update!')
print("")
print("------------------------------------------------------------------------------------------------")
print("")



with open("settings.json") as settings:
    data = json.load(settings)

    try:
        name = data["name"]
        print(Fore.GREEN + 'Loaded "name" as "'+name+'"')
    except:
        name = 'AutoLeak'
        print(Fore.RED + 'Failed to load "name", defaulted to "AutoLeak"')

    try:
        footer = data["footer"]
        print(Fore.GREEN + 'Loaded "footer" as "'+footer+'"')
    except:
        footer = '#Fortnite'
        print(Fore.RED + 'Failed to load "footer", defaulted to "#Fortnite"')

    try:
        language = data["language"]
        if language == 'ar' or language == 'de' or language == 'en' or language == 'es' or language == 'es-419' or language == 'fr' or language == 'it' or language == 'ja' or language == 'ko' or language == 'pl' or language == 'pt-BR' or language == 'de' or language == 'ru' or language == 'tr' or language == 'zh-CN' or language == 'zh-Hant':
            print(Fore.GREEN + 'Loaded "language" as "'+language+'"')
        else:
            language = 'en'
            print(Fore.YELLOW + 'Incorrect value for language was given so I have loaded "language" as "en"')

    except:
        language = 'False'
        print(Fore.RED + 'Failed to load "language", defaulted to "en"')

    try:
        imageFont = data["imageFont"]
        print(Fore.GREEN + 'Loaded "imageFont" as "'+imageFont+'"')
    except:
        imageFont = 'BurbankBigCondensed-Black.otf'
        print(Fore.RED + 'Failed to load "imageFont", defaulted to "BurbankBigCondensed-Black.otf"')

    try:
        watermark = data["watermark"]
        print(Fore.GREEN + 'Loaded "watermark" as "'+watermark+'"')
    except:
        watermark = ''
        print(Fore.RED + 'Failed to load "watermark", ignored, program will be ran without a watermark.')

    try:
        twitAPIKey = data["twitAPIKey"]
        print(Fore.GREEN + 'Loaded "twitAPIKey" as "'+twitAPIKey+'"')
    except:
        twitAPIKey = 'XXX'
        print(Fore.RED + 'Failed to load "twitAPIKey", defaulted to "XXX"')

    try:
        twitAPISecretKey = data["twitAPISecretKey"]
        print(Fore.GREEN + 'Loaded "twitAPISecretKey" as "'+twitAPISecretKey+'"')
    except:
        twitAPISecretKey = 'XXX'
        print(Fore.RED + 'Failed to load "twitAPISecretKey", defaulted to "XXX"')

    try:
        twitAccessToken = data["twitAccessToken"]
        print(Fore.GREEN + 'Loaded "twitAccessToken" as "'+twitAccessToken+'"')
    except:
        twitAccessToken = 'XXX'
        print(Fore.RED + 'Failed to load "twitAccessToken", defaulted to "XXX"')

    try:
        twitAccessTokenSecret = data["twitAccessTokenSecret"]
        print(Fore.GREEN + 'Loaded "twitAccessTokenSecret" as "'+twitAccessTokenSecret+'"')
    except:
        twitAccessTokenSecret = 'XXX'
        print(Fore.RED + 'Failed to load "twitAccessTokenSecret", defaulted to "XXX"')

    try:
        tweetUpdate = data["tweetUpdate"]
        if tweetUpdate == 'True' or tweetUpdate == 'False':
            print(Fore.GREEN + 'Loaded "tweetUpdate" as "'+tweetUpdate+'"')
        else:
            tweetUpdate = 'False'
            print(Fore.YELLOW + 'Incorrect value for tweetUpdate was given so I have loaded "tweetUpdate" as "False"')
    except:
        tweetUpdate = 'False'
        print(Fore.RED + 'Failed to load "tweetUpdate", defaulted to "False"')

    try:
        tweetAes = data["tweetAes"]
        if tweetAes == 'True' or tweetAes == 'False':
            print(Fore.GREEN + 'Loaded "tweetAes" as "'+tweetAes+'"')
        else:
            tweetAes = 'False'
            print(Fore.YELLOW + 'Incorrect value for tweetAes was given so I have loaded "tweetAes" as "False"')
    except:
        tweetAes = 'False'
        print(Fore.RED + 'Failed to load "tweetAes", defaulted to "False"')

    try:
        tweetCosmetics = data["tweetCosmetics"]
        if tweetCosmetics == 'True' or tweetCosmetics == 'False':
            print(Fore.GREEN + 'Loaded "tweetCosmetics" as "'+tweetCosmetics+'"')
        else:
            tweetCosmetics = 'False'
            print(Fore.YELLOW + 'Incorrect value for tweetCosmetics was given so I have loaded "tweetCosmetics" as "False"')
    except:
        tweetCosmetics = 'False'
        print(Fore.RED + 'Failed to load "tweetCosmetics", defaulted to "False"')

    try:
        updateCompare = data["updateCompare"]
        if updateCompare == "":
            response = requests.get('https://fortnite-api.com/v2/aes')
            updateCompare = response.json()['data']['build']
            print(Fore.YELLOW + 'No value for updateCompare was given so I have loaded "updateCompare" as "'+updateCompare+'"')
        else:
            print(Fore.GREEN + 'Loaded "updateCompare" as "'+updateCompare+'"')
    except:
        response = requests.get('https://fortnite-api.com/v2/aes')
        updateCompare = response.json()['data']['build']
        print(Fore.RED + 'Failed to load "updateCompare", I have grabbed the current build as "'+updateCompare+'"')

    try:
        aesKey = data["aesCompare"]
        if aesKey == "":
            response = requests.get('https://fortnite-api.com/v2/aes')
            aesCompare = response.json()['data']['mainKey']
            aesCompare = '0x'+aesCompare
            print(Fore.YELLOW + 'No value for aesCompare was given so I have loaded "aesCompare" as "'+aesCompare+'"')
        else:
            x = aesKey.startswith("0x")
            if x == True:
                aesCompare = aesKey
            elif x == False:
                aesCompare = '0x'+aesKey
            print(Fore.GREEN + 'Loaded "aesCompare" as "'+aesCompare+'"')
    except:
        response = requests.get('https://fortnite-api.com/v2/aes')
        aesCompare = response.json()['data']['mainKey']
        print(Fore.RED + 'Failed to load "aesCompare", I have grabbed the current build as "'+aesCompare+'"')

auth = tweepy.OAuthHandler(twitAPIKey, twitAPISecretKey)
auth.set_access_token(twitAccessToken, twitAccessTokenSecret)
api = tweepy.API(auth)

#-------------------#-------------------#

print("")
print("")


while 1:
    response = requests.get('https://fortnite-api.com/v2/aes')
    if response:
        print(Fore.YELLOW+ "Waiting for windows update -> [Count: "+str(count)+"]")
        status = response.json()["status"]
        if status == 200:
            versionLoop = response.json()["data"]["build"]
            count = count + 1
            if updateCompare != versionLoop:
                while 2:
                    print("")
                    print(Fore.GREEN+"Detected windows update! Starting "+name+"...")

                    if tweetUpdate == 'True':
                        api.update_status('['+name+'] New Update Detected!\n\n'+str(versionLoop)+'\n\n'+footer)
                        print(Fore.GREEN+"Tweeted 'Status 1' (Includes: Update notification)")

                    count = 1
                    while 3:
                        response = requests.get('https://fortnite-api.com/v2/aes')
                        if response:
                            status = response.json()["status"]
                            if status == 200:

                                print(Fore.YELLOW+ "Waiting for AES update -> [Count: "+str(count)+"]")
                                mainKey = response.json()["data"]["mainKey"]
                                mainKeyVersion1 = response.json()["data"]["build"].replace('++Fortnite+Release-', '')
                                mainKeyVersion = mainKeyVersion1.replace('-Windows', '')
                                count = count + 1

                                if aesCompare != mainKey:

                                    print(Fore.GREEN+"Detected aes update!")

                                    if tweetAes == 'True':
                                        api.update_status('['+name+'] AES Key for v'+str(mainKeyVersion)+':\n\n0x'+str(mainKey)+'\n\n'+footer)
                                        print(Fore.GREEN + "Tweeted 'Status 2' (Includes: AES key, Encrypted paks)")

                                    count = 1
                                    while 4:
                                        response = requests.get('https://fortnite-api.com/v2/cosmetics/br/new?language='+language)
                                        if response:
                                            status = response.json()["status"]
                                            if status == 200:

                                                print(Fore.YELLOW+ "Waiting for endpoint update -> [Count: "+str(count)+"]")

                                                response = requests.get('https://fortnite-api.com/v2/cosmetics/br/new?language='+language)
                                                newBuild = response.json()["data"]["build"]

                                                count = count + 1
                                                if versionLoop == newBuild:
                                                    response = requests.get('https://fortnite-api.com/v2/cosmetics/br/new?language='+language)
                                                    new = response.json()

                                                    print(Fore.BLUE + "Generating new cosmetics...")
                                                    print('')
                                                    loop = False
                                                    for i in new["data"]["items"]:

                                                        try:

                                                            print(Fore.BLUE + "Loading image for "+i["id"])
                                                            url = (i["images"]["icon"])
                                                            r = requests.get(url, allow_redirects=True)
                                                            open(f'{i["id"]}.png', 'wb').write(r.content)

                                                            img=Image.open(f'{i["id"]}.png')
                                                            img=img.resize((256,256),PIL.Image.ANTIALIAS)
                                                            img.save(f'{i["id"]}.png')


                                                            foreground = Image.open(i["id"]+'.png')
                                                            if i["rarity"]["value"] == "common":
                                                                background = Image.open('rarities/common.png')
                                                                border = Image.open('rarities/borderCommon.png')
                                                            elif i["rarity"]["value"] == "uncommon":
                                                                background = Image.open('rarities/uncommon.png')
                                                                border = Image.open('rarities/borderUncommon.png')
                                                            elif i["rarity"]["value"] == "rare":
                                                                background = Image.open('rarities/rare.png')
                                                                border = Image.open('rarities/borderRare.png')
                                                            elif i["rarity"]["value"] == "epic":
                                                                background = Image.open('rarities/epic.png')
                                                                border = Image.open('rarities/borderEpic.png')
                                                            elif i["rarity"]["value"] == "legendary":
                                                                background = Image.open('rarities/legendary.png')
                                                                border = Image.open('rarities/borderLegendary.png')
                                                            else:
                                                                background = Image.open('rarities/common.png')
                                                                border = Image.open('rarities/borderCommon.png')


                                                            Image.alpha_composite(background, foreground).save('cache/F'+i["id"]+'.png')
                                                            os.remove(i["id"]+'.png')

                                                            background = Image.open('cache/F'+i["id"]+'.png')
                                                            Image.alpha_composite(background, border).save('cache/BLANK'+i["id"]+'.png')


                                                            costype = i["type"]["displayValue"]


                                                            img=Image.open('cache/BLANK'+i["id"]+'.png')

                                                            name= i["name"]
                                                            loadFont = 'fonts/'+imageFont

                                                            font=ImageFont.truetype(loadFont,25)
                                                            w,h=font.getsize(name)
                                                            draw=ImageDraw.Draw(img)
                                                            draw.text(((18),(215)),name,font=font,fill='white')

                                                            font=ImageFont.truetype(loadFont,15)
                                                            w,h=font.getsize(costype)
                                                            draw=ImageDraw.Draw(img)
                                                            draw.text(((18),(198)),costype,font=font,fill='white')

                                                            if watermark != '':

                                                                font=ImageFont.truetype(loadFont,15)
                                                                w,h=font.getsize(watermark)
                                                                draw=ImageDraw.Draw(img)
                                                                draw.text(((20),(22)),watermark,font=font,fill='black')

                                                                font=ImageFont.truetype(loadFont,15)
                                                                w,h=font.getsize(watermark)
                                                                draw=ImageDraw.Draw(img)
                                                                draw.text(((18),(20)),watermark,font=font,fill='white')


                                                            os.remove('cache/BLANK'+i["id"]+'.png')

                                                            img.save('icons/'+i["id"]+'.png')
                                                            os.remove('cache/F'+i["id"]+'.png')

                                                            print(Fore.MAGENTA + "Generated image for "+i["id"])


                                                            if tweetCosmetics == 'True':
                                                                api.update_with_media('icons/'+i["id"]+'.png', 'Nouveau cosmétique:\n\nNom: '+i["name"]+'\nDescription: '+i["description"]+'\nType: '+costype+'\nRareté: '+i["rarity"]["displayValue"]+'\nID: '+i["id"]+'\n\n'+footer)
                                                                print(Fore.GREEN + "Tweeted image for "+i["id"])

                                                        except:
                                                            print(Fore.RED + "Ignored: '"+i["id"]+"' because of error (Possibly font issue...)")
                                                        #time.sleep(1)
                                                        print("")

                                                    print(Fore.GREEN+"")
                                                    print("!  !  !  !  !  !  !")
                                                    print("AUTOLEAK COMPLETE")
                                                    print("!  !  !  !  !  !  !")
                                                    exec(open('feed.py').read())
                                                else:
                                                    time.sleep(initialCheckDelay)

                                            else:
                                                print(Fore.RED + "Error in COSMETICS Endpoint (Status is not 200) - Retrying... (This is an error with fortnite-api.com)")
                                                time.sleep(initialCheckDelay)
                                        else:
                                            print(Fore.RED + "Error in COSMETICS Endpoint (Page Down) - Retrying... (This is an error with fortnite-api.com)")
                                            time.sleep(initialCheckDelay)

                                time.sleep(initialCheckDelay)
                            else:
                                print(Fore.RED + "Error in AES Endpoint 2 (Status is not 200) - Retrying... (This is an error with fortnite-api.com)")
                                time.sleep(initialCheckDelay)
                        else:
                            print(Fore.RED + "Error in AES Endpoint 2 (Page Down) - Retrying... (This is an error with fortnite-api.com)")
                            time.sleep(initialCheckDelay)


            time.sleep(initialCheckDelay)
        else:
            print(Fore.RED + "Error in AES Endpoint 1 (Status is not 200) - Retrying... (This is an error with fortnite-api.com)")
            time.sleep(initialCheckDelay)
    else:
        print(Fore.RED + "Error in AES Endpoint 1 (Page Down) - Retrying... (This is an error with fortnite-api.com)")
        time.sleep(initialCheckDelay)
